#!/bin/bash

# Define the file you want to copy
file_to_copy="path/to/your/file.txt"

# List all target directories with new names
directories=(
  "newfolder1"
  "newfolder2"
  "newfolder3"
  "newfolder4"
  "newfolder5"
  "newfolder6"
  "newfolder7"
  "newfolder8"
  "newfolder9"
  "newfolder10"
  "newfolder11"
  "newfolder12"
  "newfolder13"
  "newfolder14"
  "newfolder15"
  "newfolder16"
  "newfolder17"
  "newfolder18"
  "newfolder19"
  "newfolder20"

)

# Loop through each directory and copy the file
for dir in "${directories[@]}"; do
  mkdir -p "$dir" # Create the directory if it does not exist
  cp "$file_to_copy" "$dir"
done
